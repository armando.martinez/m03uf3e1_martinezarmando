﻿using System;
using System.IO;
using System.Collections.Generic;

namespace GestioDirectoris
{
    class Program
    {
        static void Main()
        {
            string r = "";
            for (int i = 0; i < Console.WindowWidth * 3; i++) { r += "/"; }
            int selectedOption;
            do
            {
                Rainbow(r);
                Console.WriteLine();
                selectedOption = Menu(new string[] { "1 - Exercici 1 ", "2 - Exercici 2 ", "3 - Exercici 3 ", "4 - Exercici 4 ", "0 - Exit " });
                if (selectedOption != 0)
                {
                    Color(ConsoleColor.Black, ConsoleColor.Gray);
                    Console.WriteLine($"\n> {Directory.GetCurrentDirectory()}\n");
                    Color();
                    Console.Write("Dona'm un path: ");
                    string path = Console.ReadLine();
                    //path = @"D:\ArmandoMartinez\LEL"; // EM CARREGO EL PATH QUE DONA EL USUARI PER FER PROVES
                    switch (selectedOption)
                    {
                    case 1:
                        if (DirFileExists(path))
                        {
                            if (File.Exists(path)) { Console.WriteLine($"\nEl fitxer \'{path}\' existeix."); }
                            else { Console.WriteLine($"\nEl directori \'{path}\' existeix"); }
                            Console.WriteLine($"\nEl path complet es: \'{Path.GetFullPath(path)}\'\n");
                        } else { Console.WriteLine($"\nNo existeix cap directori o fitxer en \'{path}\'\n"); }
                        break;
                    case 2:
                        if (Directory.Exists(path)) 
                        {
                            Console.WriteLine();
                            DirList(path);
                            Console.WriteLine();
                            }
                        else { Console.WriteLine($"\nNo existeix el directori \'{path}\'\n"); }
                        break;
                    case 3:
                        Console.WriteLine();
                        int dirOption = Menu(new string[] { "\t1 - ", "\t2 - ", "\t3 - ", "\t0 - Torna al menú principal"});
                        break;
                    case 4:
                        Directory.CreateDirectory(path + @"\XD\LMAO");
                        break;
                    case 0:
                        break;
                    }
                    Pause();
                    Console.Clear();
                }
            } while (selectedOption != 0);
            Rainbow("\n" + r);
        }

        // Mostra un menú en base a un array d'opcions
        static int Menu(string[] options)
        {
            Color(ConsoleColor.White, ConsoleColor.DarkGray);
            foreach (string option in options) { Console.WriteLine(option); }
            Color();
            int selectedOption = ParseIntInRange("\nSELECCIONA UNA OPCIÓ: ", 0, options.Length);
            return selectedOption;
        }
        // Demana una dada que obligatoriament ha d'estar dins d'un rang. Repeteix fins que es dona una dada adequada i la retorna.
        static int ParseIntInRange(string prompt, int min, int max)
        {
            bool isParsed;
            int parsedNum;
            do
            {
                Console.Write(prompt);
                string userNum = Console.ReadLine();
                isParsed = int.TryParse(userNum, out parsedNum);
                if (!isParsed) { Console.WriteLine("El format es incorrecte!"); }
                else if (parsedNum > max || parsedNum < min) { Console.WriteLine($"El número ha d'estar entre {min} i {max}!"); }
            } while (!isParsed || parsedNum > max || parsedNum < min);
            return parsedNum;
        }
        // Crea una pause en el código
        static void Pause(string prompt = "\n[PRESS ENTER TO CONTINUE]\n")
        {
            Color(ConsoleColor.Black, ConsoleColor.Gray);
            Console.Write(prompt);
            Color();
            Console.ReadLine();
        }
        // Cambia el color de la consola
        static void Color(ConsoleColor fg = ConsoleColor.White, ConsoleColor bg = ConsoleColor.Black)
        {
            Console.ForegroundColor = fg;
            Console.BackgroundColor = bg;
        }
        // Comprova si un fitxer o directori existeix. Mostra la ruta absoluta.
        static bool DirFileExists(string path)
        {
            return File.Exists(path) || Directory.Exists(path);
        }
        // Mostra un llistat de directoris i fitxers
        static void DirList(string path, int subLvl = 0)
        {
            List<string> dirs = new List<string>(Directory.EnumerateDirectories(path));
            IEnumerable<string> files = Directory.EnumerateFiles(path, "*", SearchOption.TopDirectoryOnly);
            foreach (string dir in dirs)
            {
                for (int i = 0; i < subLvl; i++) { Console.Write("\t"); }
                Color(ConsoleColor.Cyan);
                Console.Write("<DIR>");
                Color();
                Console.WriteLine($" {Path.GetFullPath(dir)}");
                DirList(dir, subLvl + 1);
            }
            foreach (string file in files)
            {
                for (int i = 0; i < subLvl; i++) { Console.Write("\t"); }
                Color(ConsoleColor.Blue);
                Console.Write("<FILE>");
                Color();
                Console.WriteLine($" {Path.GetFullPath(file)}");
            }
        }
        // XD
        static void Rainbow(string text)
        {
            ConsoleColor[] rainbow = { ConsoleColor.Magenta, ConsoleColor.Red, ConsoleColor.Yellow, ConsoleColor.Green,
                                       ConsoleColor.Cyan, ConsoleColor.Blue, ConsoleColor.DarkMagenta};
            int i = 0;
            foreach (char val in text)
            {
                Console.ForegroundColor = rainbow[i];
                Console.Write(val);
                i++;
                if (i == rainbow.Length) { i = 0; }
            }
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
